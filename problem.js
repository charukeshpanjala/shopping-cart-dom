/*
    Write a DOM application from scratch. (empty body)

    Shopping Cart -
        - It shows list of items that user wants to purchase.

        Tasks:
        1. Take input from user using input element.
        2. Add the taken input to the list.
        3. Provide a delete button to delete items from the list.
        4. Provide a way to let the user tick mark an item in the list. (Tick implies item is purchased.)
        5. Provide a way to let the user to untick an already ticked mark item. 


        Add the above abilities for bunch of users.
        Hint: 
            - Add a dropdown to select a particular user and see their shoppinglist.
            - You need to use closure or class to implement this behaviour. Feel free to use either of that.

        Also, Try to make input element controlled. (Not compulsory)

*/