function main() {
    let obj = {
        "Bob": [{ name: "Trouser", checked: false }, { name: "Shirt", checked: false }],
        "Billie": [{ name: "Jacket", checked: false }, { name: "Perfume", checked: false }],
        "Robbie": [{ name: "Jeans", checked: false }]
    }

    const addItem = (name, item) => {
        obj[name] = [...obj[name], item]
        return obj
    }

    const removeItem = (name, item) => {
        obj[name] = obj[name].filter(eachItem => eachItem !== item)
        return obj
    }

    return {
        obj, addItem, removeItem
    }
}

const getDetails = main()


// Accessing Body tag.
const body = document.getElementsByTagName("body")[0];
body.style.padding = "3em";
body.style.backgroundColor = "#DCDCDC";

// Creating form element and appending to body.
const form = document.createElement("form");
form.style.display = "flex";
body.appendChild(form);

// Creating dropdown and appending to form.
const dropdown = document.createElement("select");
dropdown.style.padding = "1em";
dropdown.style.width = "10em";
dropdown.style.cursor = "pointer";
form.appendChild(dropdown);

// adding names of the people to the dropdown.
Object.keys(getDetails.obj).forEach((name) => {
    let option = document.createElement("option");
    option.textContent = name;
    dropdown.appendChild(option);
})

// Creating an input element and appending to form.
const input = document.createElement("input");
input.setAttribute("type", "text")
input.setAttribute("placeholder", "Add Item here...")
input.style.width = "30em";
input.style.padding = "1em";
form.appendChild(input);


// Creating Submit button element and appending to form.
const button = document.createElement("button");
button.setAttribute("type", "submit")
button.style.padding = "1em";
button.style.cursor = "pointer";
button.textContent = "Submit";
form.appendChild(button);

// adding a heading to the cart
const heading = document.createElement("h1");
heading.textContent = "Cart items";
heading.style.fontFamily = "Helvetica";
body.appendChild(heading);

// Creating list to display the list of items and adding at the end of body.
const ul = document.createElement("ul");
ul.style.margin = "0";
ul.style.padding = "0";
body.appendChild(ul);

//Accesing the cart items and appending to ul, each li has a p element  and a remove button.
function displayList() {
    const liItems = document.querySelectorAll("li")
    liItems.forEach((eachItem) => {
        eachItem.remove()
    })
    const items = getDetails.obj[dropdown.value]
    items.forEach((item) => {
        const li = document.createElement("li")
        li.setAttribute("value", item.name)
        li.style.listStyle = "none";
        li.style.padding = "1em";
        li.style.fontFamily = "Helvetica";

        const span = document.createElement("span")

        const checkbox = document.createElement("input")
        checkbox.setAttribute("type", "checkbox")
        checkbox.style.marginRight = "1em"
        checkbox.setAttribute("id", item.name)
        checkbox.checked = item.checked

        const labelText = document.createElement("label")
        labelText.textContent = item.name
        labelText.setAttribute("for", item.name)
        if (item.checked) {
            labelText.style.textDecoration = "line-through";
        }
        span.appendChild(checkbox)
        span.appendChild(labelText)

        const button = document.createElement("button")
        button.textContent = "Delete"
        button.setAttribute("value", item.name)
        button.style.backgroundColor = "red"
        button.style.padding = "1em";
        button.style.border = "0px";
        button.style.color = "white";
        button.style.cursor = "pointer";
        button.style.borderRadius = "10px";

        li.style.width = "38.5em";
        li.style.display = "flex";
        li.style.justifyContent = "space-between"
        li.appendChild(span);
        li.appendChild(button);
        ul.appendChild(li);
    })
}
displayList();


function submit(event) {
    if (event) {
        event.preventDefault();
    }

    if (input.value !== "") {
        getDetails.obj[dropdown.value] = [...getDetails.obj[dropdown.value], { name: input.value, checked: false }]
    }
    input.value = ""
    displayList();
}

form.addEventListener("submit", submit);
dropdown.addEventListener("change", () => {
    displayList()
})

ul.addEventListener("click", (event) => {
    if (event.target.tagName === "BUTTON") {
        const li = event.target.parentNode
        const value = (event.target.value)
        getDetails.obj[dropdown.value] = getDetails.obj[dropdown.value].filter((item) => item.name !== value)
        li.remove()
    }
    if (event.target.tagName === "INPUT") {
        id = (event.target.id)
        getDetails.obj[dropdown.value].forEach(item => {
            if (item.name === id) {
                item.checked = !item.checked
            }

        })
        displayList()
    }
})

